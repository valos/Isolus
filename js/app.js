var Game = {
    // board's grid size and the size of each of its cell
    board: {
        size: null,
        rows: null,
        cols: null,
        cellSize: null
    },
    // players type (human or ai) and level if AI
    players: null,
    sound: false,
    startPlayer: false,

    gameover: null,

    // Read options when a new game is launched
    readLaunchOptions: function() {
        var option;

        // board size
        option = $('input[name=board-size]:checked').val().split('x');
        this.board.rows = option[0];
        this.board.cols = option[1];

        // start player
        option = $('input[name=start-player]:checked').val();
        this.startPlayer = option !== 'random' ? parseInt(option, 10) : 'random';

        this.players = [null, null];
        // player 1: human or ai (level easy, medium, hard)
        option = $('input[name=player-type-1]:checked').val().split(':');
        this.players[0] = {
            type: option[0],
            level: option[1]
        };
        
        // player 2: human or ai (level easy, medium, hard)
        option = $('input[name=player-type-2]:checked').val().split(':');
        this.players[1] = {
            type: option[0],
            level: option[1]
        };

        this.readInGameOptions();

        // compute cells size
        this.board.cellSize = this.board.size / Math.max(this.board.rows, this.board.cols);
    },

    // Options that can be changed during the game
    readInGameOptions: function() {
        var option;

        // sound FX on/off
        option = $('input[name=sound-fx]:checked').val();
        this.sound = option === 'on' ? true : false;
    },

    // Initialize and start game
    start: function() {
        $('#btn-rules').on('click', function () {
            $('#rules').attr('class', 'current');
            $('[data-position="current"]').attr('class', 'left');
        });
        $('#btn-rules-back').on('click', function () {
            $('#rules').attr('class', 'right');
            $('[data-position="current"]').attr('class', 'current');
        });
        $('#btn-options').on('click', function () {
            $('#options').attr('class', 'current');
            $('[data-position="current"]').attr('class', 'left');
        });
        $('#btn-options-back').on('click', function () {
            $('#options').attr('class', 'right');
            $('[data-position="current"]').attr('class', 'current');
        });

        Game.board.size = Math.min(window.innerHeight, window.innerWidth);

        $('#options form').sisyphus({
            onSave: function () {
                Game.readInGameOptions();
            }
        });

        $('#btn-new-game').on('click', function(e) {
            e.preventDefault();
            e.stopImmediatePropagation();

            Crafty.scene('Game');
        });

        // Start crafty
        Crafty.init(Crafty.DOM.window.width, Crafty.DOM.window.height, 'board');
        Crafty.canvas.init();
        Crafty.viewport.x = 0;
        Crafty.viewport.y = (window.innerHeight - Game.board.size) / 2;

        Crafty.scene('Loading');
    }
};

//document.addEventListener('DOMContentLoaded', Game.start, true);
window.addEventListener('load', Game.start, false);
