Isolus
======
A two-player abstract strategy board game based on [Isola](http://en.wikipedia.org/wiki/Isola_%28board_game%29) written in pure HTML5 (canvas, audio elements, data storage) + JavaScript ([jQuery](http://jquery.com/), [Underscore](http://underscorejs.org/), [Sisyphus](http://sisyphus-js.herokuapp.com/), [Crafty](http://craftyjs.com/)).

Rules
===
The goal of the game is to block the opponent by destroying all the squares which surround him before being blocked yourself.

The board initially contains squares.

Each of the two players has one piece.

To begin, each player places his piece.

Next, each turn of play is composed of two actions:
- moving his piece to a neighboring (horizontally, vertically, or diagonally) position that contains a square but not the opponent's piece
- removing any square with no piece on it.

Licensing
===
 * Isolus → AGPLv3
 * Building-Blocks → Apache License v2.0
 * Crafty → MIT or GPL
 * jQuery → MIT
 * Sisyphus → MIT
